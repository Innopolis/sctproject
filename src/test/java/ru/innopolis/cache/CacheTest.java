package ru.innopolis.cache;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.innopolis.entity.Customer;

/**
 * Created by Andrey on 11.10.2016.
 */
public class CacheTest {
    Cache cache;
    Customer customer;


    @Before
    public void setUp() throws Exception {
        this.cache = new Cache();
        this.customer = new Customer("Test", "Test", 2);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getInstance() throws Exception {
        Assert.assertNotNull("Instance cant be get", cache.getInstance());

    }

    @Test
    public void isExists() throws Exception {
        cache.putInCache(customer);
        Assert.assertTrue("Object does not exist", cache.isExists(customer));

    }

    @Test
    public void putInCache() throws Exception {
        cache.putInCache(customer);
        Assert.assertTrue("Object is not in cache", cache.cacheSize() == 1);

    }

    @Test
    public void cacheSize() throws Exception {
        cache.putInCache(customer);
        Assert.assertTrue("Cache is empty", cache.cacheSize() == 1);

    }

}