package ru.innopolis.entity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Andrey on 11.10.2016.
 */
public class DataGenerationTest {
    DataGeneration dataGeneration;
    Logger logger = LoggerFactory.getLogger(DataGenerationTest.class);

    @Before
    public void setUp() throws Exception {
        this.dataGeneration = new DataGeneration();

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getCustomerSet() throws Exception {
        logger.info("Testing getCustomerSet()");
        dataGeneration.storeTempData();
        Assert.assertTrue("Customer set is not generated", !dataGeneration.getCustomerSet().isEmpty());

    }

    @Test
    public void storeTempData() throws Exception {
        dataGeneration.storeTempData();
        Assert.assertTrue("Data are not stored", dataGeneration.getCustomerSet().size() != 0);

    }
}