package ru.innopolis.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.innopolis.cache.Cache;
import ru.innopolis.worker.FileWorker;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Andrey on 13.10.2016.
 */
public class DeserializationManagerTest {
    DeserializationManager deserializationManager;
    Cache cache;
    File file = new File("C:\\Users\\Andrey\\IdeaProjects\\SCT_Task\\src\\main\\resources\\temp.txt");
    private FileWorker fileWorker;


    @Before
    public void setUp() throws Exception {
        this.deserializationManager = new DeserializationManager();
        this.cache = Cache.getInstance();
        this.fileWorker = new FileWorker();
    }

    @Test
    public void startDeserialization() throws Exception {
        fileWorker.doWork(file.getAbsolutePath());
        Assert.assertTrue("Error while deserialize", cache.cacheSize() != 0);

    }

}