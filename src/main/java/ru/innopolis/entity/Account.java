package ru.innopolis.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Andrey on 06.10.2016.
 */
class Account implements Serializable {
    private final Customer customer;
    private long accountNumber;
    private double balance;
    private boolean status;

    Account(Customer customer, double balance, boolean status) {
        this.customer = customer;
        this.balance = balance;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return getAccountNumber() == account.getAccountNumber() &&
                Double.compare(account.getBalance(), getBalance()) == 0 &&
                isStatus() == account.isStatus() &&
                Objects.equals(getCustomer(), account.getCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCustomer(), getAccountNumber(), getBalance(), isStatus());
    }

    private Customer getCustomer() {
        return customer;
    }

    private long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    private double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    private boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
