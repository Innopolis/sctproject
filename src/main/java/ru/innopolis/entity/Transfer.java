package ru.innopolis.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Andrey on 06.10.2016.
 */
class Transfer implements Serializable {
    private final Customer fromCustomer;
    private final Customer toCostumer;
    private final Double transferAmount;
    private long transferId;

    Transfer(Customer fromCustomer, Customer toCostumer, Double transferAmount) {
        this.fromCustomer = fromCustomer;
        this.toCostumer = toCostumer;
        this.transferAmount = transferAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transfer)) return false;
        Transfer transfer = (Transfer) o;
        return getTransferId() == transfer.getTransferId() &&
                Objects.equals(getFromCustomer(), transfer.getFromCustomer()) &&
                Objects.equals(getToCostumer(), transfer.getToCostumer()) &&
                Objects.equals(getTransferAmount(), transfer.getTransferAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFromCustomer(), getToCostumer(), getTransferAmount(), getTransferId());
    }

    private Customer getFromCustomer() {
        return fromCustomer;
    }

    private Customer getToCostumer() {
        return toCostumer;
    }

    private long getTransferId() {
        return transferId;
    }

    public void setTransferId(long transferId) {
        this.transferId = transferId;
    }

    private Double getTransferAmount() {
        return transferAmount;
    }
}
