package ru.innopolis.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Andrey on 08.10.2016.
 */
public class DataGeneration {
    private static Logger logger = LoggerFactory.getLogger(DataGeneration.class);
    private Customer customer1 = new Customer("Andrey", "Zolin", 1);
    private Customer customer2 = new Customer("Sasha", "Simonenko", 2);
    private Customer customer3 = new Customer("Vladislav", "Bobko", 3);
    private Account acc1 = new Account(customer1, 23323, true);
    private Account acc2 = new Account(customer2, 23323, true);
    private Transfer tr1 = new Transfer(customer1, customer2, (double) 1000);
    private Set<Customer> customerSet = createCustomerSet();
    private Map<Account, Customer> accountRegistry = createAccountRegistry();
    private Map<Transfer, Account> transferRegistry = createTransferRegistry();

    public Set<Customer> getCustomerSet() {
        return customerSet;
    }

    private Map<Transfer, Account> createTransferRegistry() {
        return new ConcurrentHashMap<>();
    }

    private Map<Account, Customer> createAccountRegistry() {
        return new ConcurrentHashMap<>();
    }

    private Set<Customer> createCustomerSet() {
        return new HashSet<>();
    }

    /**
     * Store data in collections for testing
     */
    public void storeTempData() {
        customerSet.add(customer1);
        customerSet.add(customer2);
        customerSet.add(customer3);
        logger.info("Data was generated");
    }
}
