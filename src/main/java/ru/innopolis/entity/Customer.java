package ru.innopolis.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Andrey on 06.10.2016.
 */
public class Customer implements Serializable {
    private final String firstName;
    private final String lastName;
    private int customerId;

    public Customer(String firstName, String lastName, int customerId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.customerId = customerId;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return getCustomerId() == customer.getCustomerId() &&
                Objects.equals(getFirstName(), customer.getFirstName()) &&
                Objects.equals(getLastName(), customer.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getCustomerId());
    }

    public long getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customerId){
        this.customerId = customerId;
    }


    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {

        return firstName;
    }
}
