package ru.innopolis.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by Andrey on 06.10.2016.
 */
public class Cache {
    private static volatile Cache instance;
    private static ConcurrentMap<Integer, Object> cacheMap = new ConcurrentHashMap<>();
    private static Logger logger = LoggerFactory.getLogger(Cache.class);


    /**
     * Realization of lazy singleton
     *
     * @return instance of the object
     */
    public static Cache getInstance() {
        if (instance == null) {
            instance = new Cache();
        }
        logger.info("Get instance of the Cache");
        return instance;
    }

    /**
     * Check if object is already exists
     *
     * @param o Take object
     * @return Return true if object hash code is equal to key
     */
    public boolean isExists(Object o) {
        return cacheMap.containsKey(o.hashCode());
    }

    /**
     * Store object in hash map
     *
     * @param o take some object
     */
    public void putInCache(Object o) {
        cacheMap.putIfAbsent(o.hashCode(), o);
        logger.info("Object was added to the cache");
        logger.info("Actual cache size is {}", cacheMap.size());
    }

    /**
     * Return cache size
     *
     * @return size
     */
    public int cacheSize() {
        return cacheMap.size();
    }

}
