package ru.innopolis.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.entity.DataGeneration;
import ru.innopolis.manager.SerializationManager;

import java.io.IOException;

/**
 * Created by Andrey on 11.10.2016.
 */
public class SerializationWorker implements ResourceWorker {
    private static SerializationManager serializationManager = new SerializationManager();
    private static DataGeneration dataGeneration = new DataGeneration();

    private static Logger logger = LoggerFactory.getLogger(SerializationWorker.class);

    @Override
    public void doWork(String path) throws IOException {
        dataGeneration.storeTempData();
        logger.info("Start serialization");
        serializationManager.startSerialization(path, dataGeneration);
    }
}
