package ru.innopolis.worker;

import java.io.IOException;

/**
 * Created by Andrey on 10.10.2016.
 */
interface ResourceWorker {

    void doWork(String path) throws IOException;

}
