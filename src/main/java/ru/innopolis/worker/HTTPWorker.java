package ru.innopolis.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.manager.DeserializationManager;

import java.io.IOException;

/**
 * Created by Andrey on 11.10.2016.
 */
public class HTTPWorker implements ResourceWorker {
    private DeserializationManager deserializationManager = new DeserializationManager();
    private static Logger logger = LoggerFactory.getLogger(HTTPWorker.class);

    @Override
    public void doWork(String path) throws IOException {
        deserializationManager.startDeserialization(path);
        logger.info("Starting serialization");
    }
}
