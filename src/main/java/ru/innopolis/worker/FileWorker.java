package ru.innopolis.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.manager.DeserializationManager;

import java.io.IOException;

/**
 * Created by Andrey on 11.10.2016.
 */
public class FileWorker implements ResourceWorker {
    private static Logger logger = LoggerFactory.getLogger(FileWorker.class);
    private DeserializationManager deserializationManager = new DeserializationManager();

    public void doWork(String fileResources) throws IOException {
        try {
            deserializationManager.startDeserialization(fileResources);
            logger.info("Starting deserialization");
        } catch (IOException e) {
            logger.error("Fail to do deserialization {}", e);
        }
    }
}
