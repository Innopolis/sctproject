package ru.innopolis.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Andrey on 08.10.2016.
 */
public class GetResourceManager {
    private List<String> fileResources = new ArrayList<>();
    private static Logger logger = LoggerFactory.getLogger(GetResourceManager.class);

    /**
     * Parse resource names from line and store in collection
     *
     * @param info Take line and parse it
     */
    public void readResourcesNames(String info) {
        logger.info("Read resources");
        System.out.println(info);
        Scanner scanner = new Scanner(System.in);
        String source;
        source = scanner.nextLine();
        if (!source.isEmpty()) {
            String delimiter = "[ ]+";
            String[] tokens = source.split(delimiter);
            Collections.addAll(fileResources, tokens);
        }
        scanner.close();
    }

    public List<String> getFileResources() {
        return fileResources;
    }
}
