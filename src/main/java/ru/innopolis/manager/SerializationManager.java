package ru.innopolis.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.entity.DataGeneration;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrey on 12.10.2016.
 */
public class SerializationManager extends SerializationDeserializationManager {

    private ExecutorService executor = Executors.newCachedThreadPool();
    private static Logger logger = LoggerFactory.getLogger(SerializationManager.class);

    @Override
    public void startSerialization(String resource, DataGeneration dataGeneration) {
        Iterator iterator = dataGeneration.getCustomerSet().iterator();
        executor.submit(() -> {
            try (FileOutputStream fileOutputStream = new FileOutputStream(resource); ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                while (iterator.hasNext()) {
                    objectOutputStream.writeObject(iterator.next());
                    logger.info("Object serialized...");
                }
                objectOutputStream.writeObject(null);
                executor.shutdownNow();
                logger.info("Thread {} finished", Thread.currentThread().getName());
            } catch (Exception e) {
                executor.shutdownNow();
                logger.trace("Serialization failed {}", e.toString());
            }
        });
    }
}
