package ru.innopolis.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.cache.Cache;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrey on 12.10.2016.
 */
public class DeserializationManager extends SerializationDeserializationManager {

    private static Logger logger = LoggerFactory.getLogger(DeserializationManager.class);
    private final Cache tempCache =  Cache.getInstance();
    private ExecutorService executor = Executors.newCachedThreadPool();


    @Override
    public void startDeserialization(String resource) throws IOException {
        executor.submit(() -> {
            logger.info("Start thread {}", Thread.currentThread().getName());
            try (FileInputStream fileInputStream = new FileInputStream(resource); ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
                logger.info("Deserialize from {}", resource);
                Object o;
                synchronized (tempCache) {
                    while ((o = objectInputStream.readObject()) != null) {
                        if (tempCache.isExists(o)) {
                            logger.warn("Object exists! Exiting...");
                            executor.shutdownNow();
                            break;
                        } else {
                            tempCache.putInCache(o);
                        }
                    }
                }
                executor.shutdownNow();
                logger.info("Thread {} finished", Thread.currentThread().getName());
                fileInputStream.close();
            } catch (IOException | ClassNotFoundException e) {
                logger.trace("Object cant be deserialize", e);
                executor.shutdownNow();
            }
        });
    }

}
