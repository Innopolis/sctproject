package ru.innopolis.manager;

import ru.innopolis.entity.DataGeneration;

import java.io.IOException;

/**
 * Created by Andrey on 06.10.2016.
 */

class SerializationDeserializationManager {

    /**
     * Method which store objects into the file
     *
     * @param resource       Take path to the file
     * @param dataGeneration Take object which contain information for do serialization
     */
    public void startSerialization(String resource, DataGeneration dataGeneration) {
    }

    /**
     * Method which create threads for deserializer objects from different resources
     *
     * @param resource Take path to the file
     * @throws IOException Do exception if path is wrong
     */
    public void startDeserialization(String resource) throws IOException {
    }
}
