package ru.innopolis;

/*
 * Описание задачи:
 * Задача: Реализовать кеш объектов с возможностью сериализации и десиариализации объектов.
 * При этом данные для инициализации могут находится во множестве ресурсов,
 * каждый ресурс должен быть обработан в отдельном потоке.
 * При появлении дубликатов процесс инициализации должен быть прерван с соответствующей ошибкой.
 * Все ошибки должны быть корректно обработаны, все API покрыто модульными тестами.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.menu.MenuManager;

import java.io.IOException;


/**
 * Created by Andrey on 06.10.2016.
 */

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        logger.info("Starting application");
        MenuManager.chooseOption();
    }
}
