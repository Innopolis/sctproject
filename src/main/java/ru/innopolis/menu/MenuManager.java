package ru.innopolis.menu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.manager.GetResourceManager;
import ru.innopolis.worker.FileWorker;
import ru.innopolis.worker.HTTPWorker;
import ru.innopolis.worker.SerializationWorker;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Andrey on 12.10.2016.
 */
public class MenuManager {
    private static GetResourceManager getResourceManager = new GetResourceManager();
    private static FileWorker fileWorker = new FileWorker();
    private static HTTPWorker httpWorker = new HTTPWorker();
    private static SerializationWorker serializationWorker = new SerializationWorker();
    private static Logger logger = LoggerFactory.getLogger(MenuManager.class);


    /**
     * Method which realize menu for option choosing
     *
     * @throws IOException Throw exception if path is incorrect
     */
    public static void chooseOption() throws IOException {
        logger.info("Entering in main menu");
        System.out.println("Choose options below: \n Type 1 for 'Serialization' \n Type 2 for 'Deserialization':");
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            switch (scanner.nextInt()) {
                case 1:
                    logger.info("User choose 'Serialization' option");
                    getResourceManager.readResourcesNames("Enter whole path to file:");
                    serializationWorker.doWork(getResourceManager.getFileResources().get(0));
                    break;
                case 2:
                    logger.info("User choose 'Deserialization' option");
                    getResourceManager.readResourcesNames("Enter paths to resources divided by space:");
                    for (int i = 0; i < getResourceManager.getFileResources().size(); i++) {
                        if (getResourceManager.getFileResources().get(i).contains("http://")) {
                            httpWorker.doWork(getResourceManager.getFileResources().get(i));
                        } else if (getResourceManager.getFileResources().get(i).contains(".txt")) {
                            fileWorker.doWork(getResourceManager.getFileResources().get(i));
                        } else {
                            logger.error("Wrong input");
                        }
                    }
                    break;
                default:
                    logger.error("Wrong input");
                    break;
            }
        } else {
            logger.error("Wrong input");
        }
        scanner.close();
    }
}
